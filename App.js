import React, { Component } from 'react';
import {
  StyleSheet,
  BackHandler,
  View,
  StatusBar,
  AsyncStorage
} from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import firebase from 'react-native-firebase';
import DB from './src/helpers/DB';
var service = new DB();

import { NativeModules } from 'react-native';

// TODO: alttaki false olacak
// NativeModules.DevSettings.setIsDebuggingRemotely(false);

import {
  MainNavigator,
  MainNavigatorWithIntro,
  MainNavigatorWithoutLogin
} from './src/helpers/PageStructure';
import * as colors from './src/helpers/ColorPalette';
import { store, persistor } from './src/store';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      appReady: false
    };
    service.init();
  }

  componentDidMount = async () => {
    this.checkPermission();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.exitApp();
    });
  };

  componentWillMount = () => {
    service.checkTables();
    var result = service.select();
    if (result) {
      this.setState({ user: {}, appReady: true });
    }
    this.displayUser();
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  displayUser = async () => {
    let user = await AsyncStorage.getItem('user');
    this.setState({ user, appReady: true });
  };

  // TODO: navigatorlerin yeri değişecek
  renderPage = () => {
    const { user, appReady } = this.state;

    if (!user && appReady) {
      return <MainNavigatorWithIntro />;
    }

    if (user && appReady) {
      return <MainNavigator />;
    }
  };

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    console.log('checking token');
    var fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      await service.updateFcmToken(fcmToken);
      console.log('token: ', fcmToken);
      // user has a device token
      // await AsyncStorage.setItem('fcmToken', fcmToken);
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  render() {
    const { email, bayiNo, password } = this.state;
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle='dark-content' />
          <View style={styles.container}>{this.renderPage()}</View>
        </PersistGate>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  }
});
