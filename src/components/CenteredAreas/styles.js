import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
    centeredAreaContainerStyle: {
        width,
        alignItems: 'center',
        justifyContent: 'center'
    },
});