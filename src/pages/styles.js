import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../helpers/ColorPalette';
const { width, height } = Dimensions.get('window');
const MARGIN_VALUE = 16;

export default StyleSheet.create({
  loginPageContainerStyle: {
    width,
    height
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  loginPageTextInputStyle: {
    width: width - MARGIN_VALUE * 6,
    height: 60,
    backgroundColor: 'white',
    marginVertical: MARGIN_VALUE / 2,
    paddingHorizontal: MARGIN_VALUE,
    zIndex: 10
  },
  loginPageWarningContainerStyle: {
    width: width - MARGIN_VALUE * 6
  },
  loginPageWarningTextStyle: {
    color: 'red'
  },
  contentPageLoadingScreeneContainerStyle: {
    width,
    height,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10
  }
});
