import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  Dimensions,
  AsyncStorage,
  Platform
} from 'react-native';

const { width, height } = Dimensions.get('window');

import introImage1 from '../../assets/images/tanitimsayfasi1.png';
import introImage2 from '../../assets/images/tanitimsayfasi2.png';
import introImage3 from '../../assets/images/tanitimsayfasi3.png';
import logo from '../../assets/images/logo.png';

const HEADER_HEIGHT = 60;
const MARGIN_VALUE = 16;
const BUTTON_WIDTH = 120;

class IntroducutionPage extends Component {
  handleButtonPress = () => {
    const { navigation } = this.props;
    let user = 'Otolye';

    navigation.navigate('LoginPage');
    AsyncStorage.setItem('user', user);
  };
  render() {
    const { navigation } = this.props;
    return (
      <View style={{ flex: 1 }}>
        {Platform.OS === 'ios' ? (
          <View style={{ width, height: 40, backgroundColor: 'white' }} />
        ) : null}
        <View
          style={{
            width,
            height: HEADER_HEIGHT,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white'
          }}
        >
          <Image
            source={logo}
            style={{ height: HEADER_HEIGHT - 24, width }}
            resizeMode='contain'
          />
        </View>
        <View style={{ width, height: height - HEADER_HEIGHT }}>
          <ImageBackground
            source={introImage3}
            style={{ width, height: height - HEADER_HEIGHT }}
            resizeMode='cover'
          >
            <View
              style={{
                position: 'absolute',
                bottom: 48,
                left: width / 2 - BUTTON_WIDTH / 2
              }}
            />
          </ImageBackground>
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            zIndex: 32
          }}
        >
          <TouchableOpacity onPress={() => this.handleButtonPress()}>
            <View
              style={{
                width,
                height: 80,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 4,
                borderColor: '#19A6A6'
              }}
            >
              <Text
                style={{
                  fontWeight: '800',
                  letterSpacing: 0.5
                }}
              >
                DEVAM EDİN
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default IntroducutionPage;
