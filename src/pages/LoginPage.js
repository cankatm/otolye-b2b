import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  Animated,
  Dimensions,
  TextInput,
  Alert,
  Linking,
  AsyncStorage,
  Platform
} from 'react-native';
import { connect } from 'react-redux';

import { CenteredArea } from '../components/CenteredAreas';
import { changeEmail, changeBayiNo, changePassword } from '../actions';
import styles from './styles';

import road from '../../assets/images/road.png';
import car from '../../assets/images/car.png';
import bgImage from '../../assets/images/bgLogosuz.png';
import logo from '../../assets/images/logo.png';

import DB from '../helpers/DB';
var service = new DB();

const CAR_WIDTH = 100;
const MARGIN_VALUE = 16;

const { width, height } = Dimensions.get('window');

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      bayiNo: '',
      password: '',
      animation: new Animated.Value(0),
      passwordForgotPressed: false,
      appReady: false
    };
    // service.dropTable();
    service.init();
  }

  componentDidMount = () => {
    this.startAnimation();
  };

  componentWillMount = async () => {
    const { navigation } = this.props;
    service.checkTables();

    var result = await service.select();

    console.log(result);
    if (result) {
      navigation.navigate('ContentPage', {
        email: result.user_email,
        bayiNo: result.user_bayiNo,
        password: result.user_password
      });
      this.setState({
        appReady: true,
        email: result.user_email,
        bayiNo: result.user_bayiNo,
        password: result.user_password
      });
    } else {
      this.setState({ appReady: true });
    }
  };

  startAnimation = () => {
    Animated.timing(this.state.animation, {
      toValue: 1,
      duration: 2500
    }).start(({ finished }) => {
      if (finished) {
        Animated.timing(this.state.animation, {
          toValue: 0,
          duration: 0
        }).start(() => {
          this.startAnimation();
        });
      }
    });
  };

  handleLinkPress = async (url, fromForgotPassword) => {
    const { email, bayiNo } = this.state;

    if (!fromForgotPassword) {
      Linking.openURL(url).catch(err =>
        console.error('An error occurred', err)
      );
    } else if (fromForgotPassword && email.length > 0 && bayiNo.length > 0) {
      var data = `email=${email}&dealerNo=${bayiNo}`;

      var xhr = new XMLHttpRequest();
      xhr.withCredentials = true;

      xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState === 4) {
          //console.log('cookie')
          console.log('aaaaaaaa', xhr.getResponseHeader('Set-Cookie'));
          console.log('aaaaaaaa', xhr.responseText);
        }
      });

      xhr.open(
        'POST',
        'https://dev.otolye.com/tunc/b2b/login?req=passRecovery'
      );
      xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
      xhr.send(data);
      Alert.alert(
        'Şifremi unuttum',
        'Yenileme linki gönderildi',
        [{ text: 'Tamam', onPress: () => console.log('Ask me later pressed') }],
        { cancelable: true }
      );
    } else if (fromForgotPassword) {
      this.setState({ passwordForgotPressed: true });
    }
  };

  handleLoginPress = async () => {
    const { navigation } = this.props;

    const { email, bayiNo, password } = this.state;

    if (email == '' || bayiNo == '' || password == '') {
      Alert.alert(
        'Eksik Alan',
        'Eksik alanları doldurunuz',
        [{ text: 'Tamam', onPress: () => console.log('Ask me later pressed') }],
        { cancelable: true }
      );
    } else {
      await service.updateLoginData(email, bayiNo, password);
      this.setState({ passwordForgotPressed: false });
      navigation.navigate('ContentPage', { email, bayiNo, password });
    }
  };

  render() {
    const {
      animation,
      passwordForgotPressed,
      email,
      bayiNo,
      password
    } = this.state;

    const animatedInterpolate = animation.interpolate({
      inputRange: [0, 1],
      outputRange: [-100, width]
    });

    const animatedStyles = {
      left: animatedInterpolate
    };

    if (!this.state.appReady) {
      return <View style={{ width, height, backgroundColor: 'white' }} />;
    }

    return (
      <View style={styles.loginPageContainerStyle}>
        {Platform.OS === 'ios' ? (
          <View style={{ width, height: 40, backgroundColor: 'white' }} />
        ) : null}
        <ImageBackground
          source={bgImage}
          style={styles.loginPageContainerStyle}
          resizeMode='cover'
        >
          <CenteredArea marginTop={16}>
            <Image
              source={logo}
              style={{ width: 100, height: 40 }}
              resizeMode='contain'
            />
          </CenteredArea>

          {passwordForgotPressed && email.length < 1 && (
            <CenteredArea>
              <View style={styles.loginPageWarningContainerStyle}>
                <Text style={styles.loginPageWarningTextStyle}>
                  Zorunlu Alan
                </Text>
              </View>
            </CenteredArea>
          )}

          <View style={{ zIndex: 10 }}>
            <CenteredArea>
              <TextInput
                style={styles.loginPageTextInputStyle}
                onChangeText={value => this.setState({ email: value })}
                value={email}
                maxLength={50}
                placeholder='E-Posta'
                placeholderTextColor='black'
                selectionColor='black'
                autoCorrect={false}
                underlineColorAndroid='transparent'
                blurOnSubmit
                returnKeyType='done'
                secureTextEntry={false}
                keyboardType='email-address'
              />
            </CenteredArea>

            {passwordForgotPressed && bayiNo.length < 1 && (
              <CenteredArea>
                <View style={styles.loginPageWarningContainerStyle}>
                  <Text style={styles.loginPageWarningTextStyle}>
                    Zorunlu Alan
                  </Text>
                </View>
              </CenteredArea>
            )}

            <CenteredArea>
              <TextInput
                style={styles.loginPageTextInputStyle}
                onChangeText={value => this.setState({ bayiNo: value })}
                value={bayiNo}
                maxLength={50}
                placeholder='Bayi No'
                placeholderTextColor='black'
                selectionColor='black'
                autoCorrect={false}
                underlineColorAndroid='transparent'
                blurOnSubmit
                returnKeyType='done'
                secureTextEntry={false}
              />
            </CenteredArea>

            <CenteredArea>
              <TextInput
                style={styles.loginPageTextInputStyle}
                onChangeText={value => this.setState({ password: value })}
                value={password}
                maxLength={50}
                placeholder='Şifre'
                placeholderTextColor='black'
                selectionColor='black'
                autoCorrect={false}
                underlineColorAndroid='transparent'
                blurOnSubmit
                returnKeyType='done'
                secureTextEntry={true}
              />
            </CenteredArea>

            <CenteredArea>
              <TouchableOpacity onPress={() => this.handleLoginPress()}>
                <View
                  style={[
                    styles.loginPageTextInputStyle,
                    {
                      backgroundColor: '#65C1D3',
                      justifyContent: 'center'
                    }
                  ]}
                >
                  <Text style={{ fontSize: 16, color: 'white' }}>Giriş</Text>
                </View>
              </TouchableOpacity>
            </CenteredArea>

            <CenteredArea>
              <View
                style={{
                  width: width - MARGIN_VALUE * 6,
                  backgroundColor: 'transparent',
                  marginVertical: MARGIN_VALUE / 2,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  zIndex: 10
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.handleLinkPress(
                      'https://dev.otolye.com/tunc/b2b/static?req=register',
                      false
                    )
                  }
                >
                  <Text
                    style={{
                      fontSize: 14,
                      textDecorationLine: 'underline'
                    }}
                  >
                    Kaydol
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() =>
                    this.handleLinkPress(
                      'bayi.otolye.com/login?req=passRecovery',
                      true
                    )
                  }
                >
                  <Text
                    style={{
                      fontSize: 14,
                      textDecorationLine: 'underline'
                    }}
                  >
                    Şifremi Unuttum
                  </Text>
                </TouchableOpacity>
              </View>
            </CenteredArea>
          </View>

          <View
            style={{
              position: 'absolute',
              bottom: 32,
              left: 0,
              backgroundColor: 'transparent',
              justifyContent: 'flex-end'
            }}
          >
            <Image
              source={road}
              style={{ width, height: width / 1.7 }}
              resizeMode='contain'
            />
          </View>

          <Animated.View
            style={[
              {
                position: 'absolute',
                bottom: 32,
                backgroundColor: 'transparent',
                justifyContent: 'flex-end'
              },
              animatedStyles
            ]}
          >
            <Image
              source={car}
              style={{ width: CAR_WIDTH, height: CAR_WIDTH }}
              resizeMode='contain'
            />
          </Animated.View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { email, bayiNo, password } = state.formReducer;

  return { email, bayiNo, password };
};

export default connect(
  mapStateToProps,
  { changeEmail, changeBayiNo, changePassword }
)(LoginPage);
