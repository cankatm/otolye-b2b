import React, { Component } from 'react';
import {
  Text,
  View,
  //WebView,
  Animated,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  Linking,
  KeyboardAvoidingView,
  BackHandler,
  AppState,
  Platform
} from 'react-native';
import { WebView } from 'react-native-webview';

import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import firebase from 'react-native-firebase';
import DB from '../helpers/DB';
var service = new DB();

// import { changeEmail, changeBayiNo, changePassword } from '../actions';
const { width, height } = Dimensions.get('window');

import exit from '../../assets/images/exit.png';
import road from '../../assets/images/road.png';
import car from '../../assets/images/car.png';
import bgImage from '../../assets/images/bgLogosuz.png';
import logo from '../../assets/images/logo.png';

const CAR_WIDTH = 100;
const MARGIN_VALUE = 16;

const HEADER_HEIGHT = 60;

const CHATLINK = 'https://dev.otolye.com/tunc/b2b/chat?mobileapp=1';

// TODO: redux ve asyncstorage ayarlanacak
import * as colors from '../helpers/ColorPalette';
import styles from './styles';

class ContentPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      key: 1,
      loading: true,
      error: false,
      animation: new Animated.Value(0),
      cookies: {},
      cookie2: '',
      webViewUrl: CHATLINK,
      chatReady: false,
      appState: AppState.currentState,
      isBackground: false
    };
    service.init();
  }

  setLogoutToken = async token => {
    await service.updateToken(token);
    // await AsyncStorage.setItem('logoutToken', token);
  };

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.exitApp();
    });
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    this.backHandler.remove();
    // clearTimeout(this.timeOutVar);
  }

  _handleAppStateChange = nextAppState => {
    this.setState({ appState: nextAppState });

    if (nextAppState === 'background') {
      this.setState({ isBackground: true });
      console.log('App is in Background Mode.');
    }

    if (nextAppState === 'active') {
      this.setState({ isBackground: false });
      console.log('App is in Active Foreground Mode.');
    }
  };

  componentWillMount = async () => {
    this.startAnimation();

    const result = await service.select();
    const email = result.user_email;
    const bayiNo = result.user_bayiNo;
    const password = result.user_password;
    let fcmToken = result.fcm_token;

    if (result.fcmToken == null) {
      fcmToken = await firebase.messaging().getToken();
    }

    console.log('contentPage2:  ', email, bayiNo, password, fcmToken);

    var data = `email=${email}&dealerNo=${bayiNo}&pass=${password}&fci=${fcmToken}`;

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', () => {
      if (xhr.readyState === 4) {
        //console.log('cookie')
        console.log(xhr.getResponseHeader('Set-Cookie'));
        console.log(xhr.responseText);
        var result = JSON.parse(xhr.responseText);
        console.log('server cevap: ', result);
        if (result.login) {
          this.setLogoutToken(result.token);
          this.setCookie(xhr.getResponseHeader('Set-Cookie'));
          // this.timeOutVar = setTimeout(() => {
          //   this.setState({ chatReady: true });
          // }, 1000);
          this.setState({ chatReady: true });
        } else {
          console.log('hatali giris');
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'LoginPage' })]
          });

          service.remove();

          this.props.navigation.dispatch(resetAction);
          Alert.alert('Hatalı Bilgi', 'Hatalı bilgi girdiniz, tekrar deneyin', [
            {
              text: 'Tamam',
              onPress: () => console.log('Ok pressed')
            }
          ]);
        }
      }
    });

    xhr.open('POST', 'https://dev.otolye.com/tunc/b2b/login?req=mobile');
    xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
    xhr.send(data);
  };

  startAnimation = () => {
    Animated.timing(this.state.animation, {
      toValue: 1,
      duration: 2000
    }).start(({ finished }) => {
      console.log('Animation 1 DONE');
      if (finished) {
        Animated.timing(this.state.animation, {
          toValue: 0,
          duration: 0
        }).start(() => {
          console.log('Animation 2 DONE');
          this.startAnimation();
        });
      }
    });
  };

  renderLoadingScreene = () => {
    const { loading, error } = this.state;

    const animatedInterpolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [-100, width]
    });

    const animatedStyles = {
      left: animatedInterpolate
    };

    if (loading) {
      return (
        <View style={styles.contentPageLoadingScreeneContainerStyle}>
          <View
            style={{
              width,
              height: HEADER_HEIGHT,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'white',
              position: 'absolute',
              top: 0,
              left: 0,
              borderBottomWidth: 1,
              borderColor: '#65C1D3'
            }}
          >
            <Image
              source={logo}
              style={{ height: HEADER_HEIGHT - 24, width }}
              resizeMode='contain'
            />

            <View style={{ position: 'absolute', right: 0, top: 0 }}>
              <TouchableOpacity onPress={() => this.handleLogoutButton()}>
                <View
                  style={{
                    width: HEADER_HEIGHT,
                    height: HEADER_HEIGHT,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Image
                    source={exit}
                    style={{
                      width: HEADER_HEIGHT - 16,
                      height: HEADER_HEIGHT - 16
                    }}
                    resizeMode='contain'
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              position: 'absolute',
              bottom: 32,
              left: 0,
              backgroundColor: 'transparent',
              justifyContent: 'flex-end'
            }}
          >
            <Image
              source={road}
              style={{ width, height: width / 1.7 }}
              resizeMode='contain'
            />
          </View>

          <Animated.View
            style={[
              {
                position: 'absolute',
                bottom: 32,
                backgroundColor: 'transparent',
                justifyContent: 'flex-end'
              },
              animatedStyles
            ]}
          >
            <Image
              source={car}
              style={{ width: CAR_WIDTH, height: CAR_WIDTH }}
              resizeMode='contain'
            />
          </Animated.View>
        </View>
      );
    } else if (error) {
      return (
        <View style={styles.contentPageLoadingScreeneContainerStyle}>
          <View
            style={{
              width,
              height: HEADER_HEIGHT,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'white',
              position: 'absolute',
              top: 0,
              left: 0,
              borderBottomWidth: 1,
              borderColor: '#65C1D3'
            }}
          >
            <Image
              source={logo}
              style={{ height: HEADER_HEIGHT - 24, width }}
              resizeMode='contain'
            />

            <View style={{ position: 'absolute', right: 0, top: 0 }}>
              <TouchableOpacity onPress={() => this.handleLogoutButton()}>
                <View
                  style={{
                    width: HEADER_HEIGHT,
                    height: HEADER_HEIGHT,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Image
                    source={exit}
                    style={{
                      width: HEADER_HEIGHT - 16,
                      height: HEADER_HEIGHT - 16
                    }}
                    resizeMode='contain'
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <Text>Bir hata oluştu, çıkış yapıp bilgilerinizi tekrar giriniz</Text>
        </View>
      );
    } else {
    }
  };

  setCookie = c => {
    this.setState({ cookie2: c });
  };

  handleURI = () => {
    return CHATLINK;
  };

  handleLogoutButton = async () => {
    const { changeEmail, changeBayiNo, changePassword } = this.props;

    Alert.alert(
      'Çıkış Yap',
      'Çıkış yapmak istediğinize emin misiniz?',
      [
        {
          text: 'İptal',
          onPress: () => console.log('Ask me later pressed')
        },
        {
          text: 'Çıkış Yap',
          onPress: async () => {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'LoginPage' })]
            });

            var result = await service.select();
            service.remove();
            // console.log('logout', result)
            var xhr = new XMLHttpRequest();
            xhr.open(
              'GET',
              `https://dev.otolye.com/tunc/b2b/logout?req=mobile&token=${result.user_token}`
            );
            xhr.setRequestHeader(
              'content-type',
              'application/x-www-form-urlencoded'
            );
            xhr.send();

            this.props.navigation.dispatch(resetAction);
          }
        }
      ],
      { cancelable: false }
    );
  };

  onNavigationStateChange = (webViewState: { url: string }) => {
    const { url } = webViewState;
    console.log(url);
    if (!url.includes(CHATLINK)) {
      console.log('ww change', url);
      this.setState({ webViewUrl: CHATLINK, key: this.state.key + 1 });
    }

    if (this.state.key > 10) {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'LoginPage' })]
      });
      service.remove();

      this.props.navigation.dispatch(resetAction);
    }
    // if (url.includes('login')) {
    // const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [NavigationActions.navigate({ routeName: 'LoginPage' })]
    // });
    // service.remove();

    // this.props.navigation.dispatch(resetAction);
    // }
  };

  _checkNeededCookies = () => {
    const { cookies, webViewUrl } = this.state;

    if (webViewUrl === 'SUCCESS_URL') {
      if (cookies['cookie-name-for-jwt']) {
        //alert(cookies['cookie-name-for-jwt']);
        // do your magic...
      }
    }
  };

  _onMessage = event => {
    const { data } = event.nativeEvent;
    const cookies = data.split(';'); // `csrftoken=...; rur=...; mid=...; somethingelse=...`

    cookies.forEach(cookie => {
      const c = cookie.trim().split('=');

      const new_cookies = this.state.cookies;
      new_cookies[c[0]] = c[1];

      this.setState({ cookies: new_cookies });
    });

    this._checkNeededCookies();
  };

  _renderChat = () => {
    if (this.state.chatReady) {
      const jsCode = `document.cookie=\'${this.state.cookie2}\'`;

      return (
        <WebView
          key={this.state.key}
          sharedCookiesEnabled={true}
          source={{ uri: this.state.webViewUrl }}
          style={{ backgroundColor: colors.white }}
          // onError={() => {
          //   console.log('load error');
          //   this.setState({
          //     loading: false,
          //     error: true,
          //     webViewUrl: CHATLINK
          //   });
          // const resetAction = StackActions.reset({
          //   index: 0,
          //   actions: [NavigationActions.navigate({ routeName: 'LoginPage' })]
          // });

          // service.remove();

          // this.props.navigation.dispatch(resetAction);

          // Alert.alert('Hata Oluştu', 'Tekrar giriş yapmayı deneyin', [
          //   {
          //     text: 'Tamam',
          //     onPress: () => console.log('Ok pressed')
          //   }
          // ]);
          // }}
          onLoad={() => {
            this.setState({
              loading: false,
              webViewUrl: CHATLINK
            });
          }}
          // onError={() => {
          //   const resetAction = StackActions.reset({
          //     index: 0,
          //     actions: [NavigationActions.navigate({ routeName: 'LoginPage' })]
          //   });

          //   service.remove();

          //   this.props.navigation.dispatch(resetAction);

          //   Alert.alert('Hata Oluştu', 'Tekrar giriş yapmayı deneyin', [
          //     {
          //       text: 'Tamam',
          //       onPress: () => console.log('Ok pressed')
          //     }
          //   ]);
          // }}
          onLoadEnd={() => this.setState({ state: this.state })} //sayfayı yenileme denemesi
          // onError={syntheticEvent => {
          //   const { nativeEvent } = syntheticEvent;
          //   console.warn('WebView error: ', nativeEvent);
          // }}
          onNavigationStateChange={this.onNavigationStateChange}
          onMessage={this._onMessage}
          javaScriptEnabled={true}
          injectedJavaScript={jsCode}
          useWebKit={true}
        />
      );
    }
    return null;
  };

  render() {
    const { email, bayiNo, password } = this.props;
    //const jsCode = 'window.postMessage(document.cookie)';
    //const jsCode = 'document.cookie=\'asp.net_sessionid_b2b_test=dab9908f5b24f0f09e523a69cf7ecd32;testb2bpsc=lgkn1ckkprgokkkla4d2v5q41h;lang=tr;expires=28799\''

    // const patchPostMessageFunction = function() {
    //   var originalPostMessage = window.postMessage;

    //   var patchedPostMessage = function(message, targetOrigin, transfer) {
    //     originalPostMessage(message, targetOrigin, transfer);
    //   };

    //   patchedPostMessage.toString = function() {
    //     return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
    //   };

    //   window.postMessage = patchedPostMessage;
    // };
    // const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';

    return (
      <View style={{ flex: 1 }}>
        {Platform.OS === 'ios' ? (
          <View style={{ width, height: 40, backgroundColor: 'white' }} />
        ) : null}
        {this.renderLoadingScreene()}
        <View
          style={{
            width,
            height: HEADER_HEIGHT,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white',
            borderBottomWidth: 1,
            borderColor: '#65C1D3'
          }}
        >
          <Image
            source={logo}
            style={{ height: HEADER_HEIGHT - 24, width }}
            resizeMode='contain'
          />

          <View style={{ position: 'absolute', right: 0, top: 0 }}>
            <TouchableOpacity onPress={() => this.handleLogoutButton()}>
              <View
                style={{
                  width: HEADER_HEIGHT,
                  height: HEADER_HEIGHT,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Image
                  source={exit}
                  style={{
                    width: HEADER_HEIGHT - 16,
                    height: HEADER_HEIGHT - 16
                  }}
                  resizeMode='contain'
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        {!this.state.isBackground && this._renderChat()}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { email, bayiNo, password } = state.formReducer;

  return { email, bayiNo, password };
};

export default connect(
  mapStateToProps
  // { changeEmail, changeBayiNo, changePassword }
)(ContentPage);
