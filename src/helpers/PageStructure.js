import { createStackNavigator } from 'react-navigation';

import {
  IntroducutionPage,
  LoginPage,
  ContentPage,
  ContentPageLoggedIn
} from '../pages';

//Animation
const FadeTransition = (index, position) => {
  const sceneRange = [index - 1, index];
  const outputOpacity = [0, 1];
  const transition = position.interpolate({
    inputRange: sceneRange,
    outputRange: outputOpacity
  });

  return {
    opacity: transition
  };
};

//Animation navigation config
const NavigationConfig = () => {
  return {
    screenInterpolator: sceneProps => {
      const position = sceneProps.position;
      const scene = sceneProps.scene;
      const index = scene.index;

      return FadeTransition(index, position);
    }
  };
};

export const MainNavigatorWithIntro = createStackNavigator(
  {
    IntroducutionPage: { screen: IntroducutionPage },
    LoginPage: { screen: LoginPage },
    ContentPage: { screen: ContentPage },
    ContentPageLoggedIn: { screen: ContentPageLoggedIn }
  },
  {
    transitionConfig: NavigationConfig,
    headerMode: 'none',
    lazyLoad: true,
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

export const MainNavigator = createStackNavigator(
  {
    ContentPage: { screen: ContentPage },
    LoginPage: { screen: LoginPage },
    ContentPageLoggedIn: { screen: ContentPageLoggedIn }
  },
  {
    transitionConfig: NavigationConfig,
    headerMode: 'none',
    lazyLoad: true,
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

export const MainNavigatorWithoutLogin = createStackNavigator(
  {
    ContentPage: { screen: ContentPage },
    ContentPageLoggedIn: { screen: ContentPageLoggedIn },
    LoginPage: { screen: LoginPage }
  },
  {
    transitionConfig: NavigationConfig,
    headerMode: 'none',
    lazyLoad: true,
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);
