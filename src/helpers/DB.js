import SQLite from 'react-native-sqlite-storage';
import websqlite from 'websqlite';
let SqlService = new websqlite();

export default class DB {
  async init() {
    await SqlService.init({
      id: 'otolye_db', // veritabanı ismi
      dbObject: SQLite, // veritabanı nesnesi
      version: '1.4'
    });
  }

  async checkTables() {
    await SqlService.query(
      'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY, user_email VARCHAR(30), user_bayiNo VARCHAR(30), user_password VARCHAR(30), user_token VARCHAR(50), fcm_token VARCHAR(500), user_cookie VARCHAR(500), isIntroPassed INTEGER DEFAULT 0)'
    );
  }

  async dropTable() {
    await SqlService.query('DROP TABLE table_user');
  }

  async select() {
    var result = await SqlService.select('table_user', '*');
    if (result.length == 0) {
      /*
      SqlService.insert(
        'table_user',
        [
          'user_id',
          'user_email',
          'user_bayiNo',
          'user_password',
          'user_token',
          'isIntroPassed'
        ],
        [1, '', '', '', '', 1]
      );
      */
      return null;
    } else {
      return result[0];
    }
  }

  async remove() {
    await SqlService.delete('table_user', 'user_id = ?', [1]);
  }

  async updateLoginData(email, bayiNo, password) {
    var result = await SqlService.select('table_user', '*');
    if (result.length == 0) {
      SqlService.insert(
        'table_user',
        [
          'user_id',
          'user_email',
          'user_bayiNo',
          'user_password',
          'user_token',
          'fcm_token',
          'user_cookie',
          'isIntroPassed'
        ],
        [1, email, bayiNo, password, '', '', '', 1]
      );
    } else {
      await SqlService.update(
        'table_user',
        ['user_email', 'user_bayiNo', 'user_password'],
        [email, bayiNo, password],
        'user_id = ?',
        [1]
      );
    }
  }

  async updateCookie(cookie) {
    await SqlService.update(
      'table_user',
      ['user_cookie'],
      [cookie],
      'user_id = ?',
      [1]
    );
  }

  async updateToken(token) {
    await SqlService.update(
      'table_user',
      ['user_token'],
      [token],
      'user_id = ?',
      [1]
    );
  }

  async updateFcmToken(token) {
    await SqlService.update(
      'table_user',
      ['fcm_token'],
      [token],
      'user_id = ?',
      [1]
    );
  }
}
